+ DB info:
- User, Event and UserEventMapping, Delayed::Job tables are used.
- Delayed jobs are used for async calls.
- rake job is resposible to pull the data from Excel sheet.

+ Setup:
- run `rake db:setup`
- run `bundle install`
- run `rake jobs:work`
- open new tab and run `rake upload_csv` to seed the data.
- run `rails s`
- open `http://localhost:3000`


+ NOTE:
- Added both csvs to public folder
- Used UserEventMapping join table to handle user <-> event many to many relationship.
- Used rsvp enum in user_event_mappings table and added a default value to 'maybe'.
- Used delayed_job to create user_event_mapping call async.
- Due to lake of time, not able to write test cases.
