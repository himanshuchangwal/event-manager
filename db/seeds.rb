# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(username: 'jarrod.kunze', email: 'reie@ratke.net', phone: '1-573-699-0961 x2352')

event = Event.create(title: 'Philadelphia maki Fest', start_time: '2019-01-18 10:45:11 +0530', end_time: '2019-01-18 08:45:11 +0530', description: 'Milk')

UserEventMapping.create(user: user, event: event, rsvp: 1)
