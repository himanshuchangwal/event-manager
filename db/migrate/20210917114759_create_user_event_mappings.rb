class CreateUserEventMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :user_event_mappings do |t|
      t.integer :user_id
      t.integer :event_id
      t.column :rsvp, :integer, default: 0

      t.timestamps
    end
  end
end
