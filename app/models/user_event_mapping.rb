class UserEventMapping < ApplicationRecord
  belongs_to :user
  belongs_to :event

  enum rsvp: { maybe: 0, yes: 1, no: 2 }

  before_save :handle_overlapping_events

  def handle_overlapping_events
    return true if rsvp != 'yes'

    events = user.events
                 .where('start_time >= ? AND end_time <= ? OR start_time >= ? AND end_time <= ?
                         OR start_time >= ? AND end_time <=? ',
                        event.start_time, event.start_time, event.end_time, event.end_time,
                        event.start_time, event.end_time)
    # rubocop:disable Rails/SkipsModelValidations
    user.user_event_mappings.where(event: events, rsvp: 'yes')&.update_all(rsvp: 'no')
    # rubocop:enable Rails/SkipsModelValidations
  end
end
