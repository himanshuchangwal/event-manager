class User < ApplicationRecord
  has_many :user_event_mappings
  has_many :events, through: :user_event_mappings
end
