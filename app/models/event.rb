class Event < ApplicationRecord
  attr_accessor :all_day

  has_many :user_event_mappings
  has_many :users, through: :user_event_mappings

  before_validation :filter_all_date, on: :create
  validate :end_time_with_start_time

  def end_time_with_start_time
    return true if end_time.blank? || start_time.blank?

    errors.add(:end_time, 'should be greater than the start time') if end_time < start_time
  end

  def filter_all_date
    return unless all_day

    self.start_time = start_time.beginning_of_day
    self.end_time = end_time.end_of_day
  end
end
