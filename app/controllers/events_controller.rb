class EventsController < ApplicationController
  before_action :find_user
  def index
    @events = @user.events
  end

  private

  def find_user
    @user = User.find(params[:user_id])
  end
end
