require 'csv'

desc 'This will import all the csv file contents...'
task upload_csv: :environment do
  ## batch insertion without validation for Users.
  puts 'importing users data started...'
  import_users
  puts 'importing users data completed...'

  ## used delayed_job to make it async
  puts 'importing events data started...'
  import_events
  puts 'importing events data completed...'
end

def import_users
  batch = []
  batch_size = 1_000

  CSV.foreach('public/users.csv', headers: true) do |row|
    batch << User.new(row.to_hash)

    if batch.size >= batch_size
      User.import batch
      batch = []
    end
  end
  User.import batch
end

def fetch_invitees(invitees)
  invitees.parse_csv(col_sep: ';').map { |x| x.split('#') }
end

def event_params(row)
  {
    title: row['title'], description: row['description'],
    start_time: row['starttime'], end_time: row['endtime'],
    all_day: ActiveModel::Type::Boolean.new.cast(row['allday'])
  }
end

def import_events
  failing_events = {}
  CSV.foreach('public/events.csv', headers: true) do |row|
    row_invitees = row.delete('users#rsvp')&.last
    next if row_invitees.nil?

    invitees = fetch_invitees(row_invitees)
    event = Event.new(event_params(row))
    event.save ? invite_users(invitees, event) : failing_events[row['title']] = event.errors.full_messages
  end
  puts 'Failed Events......'
  puts failing_events
end

def invite_users(invitees, event)
  invitees.each do |username, rsvp|
    user = User.find_by(username: username)
    next if user.nil?

    UserEventMapping.delay.create(event: event, user: user, rsvp: rsvp)
  end
end
